package retornoDePedidoTest

import (
	"fmt"
	"layout-entire/v_3_2/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderRetorno(t *testing.T) {

	cabecalho := retornoDePedido.GetCabecalho("61280433301222","1","612804333012221")

	if cabecalho != "161280433301222161280433301222100" {
		t.Error("cabecalho não é compativel")

	}

}

func TestDadosRetorno(t *testing.T) {
	data := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)
	hora := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)

	dados := retornoDePedido.GetDados(data,hora)

	if dados != "201022000123001000000000000000000" {
		fmt.Println("dados",dados)
		t.Error("Dados não é compativel")

	}

}

func TestItensRetorno(t *testing.T) {

	itens := retornoDePedido.GetItens("1313131313131",88888888,88888888,22,1  )

	if itens != "313131313131318888888888888888221" {
		fmt.Println("itens",itens)
		t.Error("itens não é compativel")

	}

}

func TestFimRetorno(t *testing.T) {

	Fim := retornoDePedido.GetFim(88888888,77777777,99999999)

	if Fim != "488888888777777779999999900000000" {
		fmt.Println("Fim",Fim)
		t.Error("Fim não é compativel")

	}

}


