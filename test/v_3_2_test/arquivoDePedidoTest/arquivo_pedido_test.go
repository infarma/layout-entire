package arquivoDePedidoTest

import (
	"fmt"
	"layout-entire/v_3_2/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {
	f, err := os.Open("../fileTest/SDZ23455090000100_000000002710979.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var cabecalho arquivoDePedido.Cabecalho
	cabecalho.TipoRegistro = 1
	cabecalho.CnpjCliente = "18757090000206"
	cabecalho.TipoFaturamento = 1
	cabecalho.ApontadorPromoccao = "0000000000000"
	cabecalho.CodigoControle = "00"

	if cabecalho.TipoRegistro != arq.Cabecalho.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if cabecalho.CnpjCliente != arq.Cabecalho.CnpjCliente {
		t.Error("Cnpj Cliente não é compativel")
	}
	if cabecalho.TipoFaturamento != arq.Cabecalho.TipoFaturamento {
		t.Error("Tipo Faturamento não é compativel")
	}
	if cabecalho.ApontadorPromoccao != arq.Cabecalho.ApontadorPromoccao {
		t.Error("Apontador Promoccao não é compativel")
	}
	if cabecalho.CodigoControle != arq.Cabecalho.CodigoControle {
		t.Error("Codigo Controle não é compativel")
	}

	var dados arquivoDePedido.Dados1
	dados.TipoRegistro = 2
	dados.CodigoProjeto = ""
	dados.Pedido = "000000002710979"
	dados.CnpjCentroDistribuicao = "23455090000100"
	dados.SemUtilizacao = "0000"

	if dados.TipoRegistro != arq.Dados1.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if dados.CodigoProjeto != arq.Dados1.CodigoProjeto {
		t.Error("Codigo Projeto não é compativel")
	}
	if dados.Pedido != arq.Dados1.Pedido {
		t.Error("Pedido não é compativel")
	}
	if dados.CnpjCentroDistribuicao != arq.Dados1.CnpjCentroDistribuicao {
		t.Error("Cnpj Centro Distribuicao não é compativel")
	}
	if dados.SemUtilizacao != arq.Dados1.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

	var faturamento arquivoDePedido.Faturamento
	faturamento.TipoRegistro = 3
	faturamento.TipoPagamento = 2
	faturamento.CodigoPrazoDeterminado = "0000"
	faturamento.NumeroDiasPrazoDeterminado = 0
	faturamento.SemUtilizacao = "00000000000000000000000000"

	if faturamento.TipoRegistro != arq.Faturamento.TipoRegistro {
		t.Error(" não é compativel")
	}
	if faturamento.TipoPagamento != arq.Faturamento.TipoPagamento {
		t.Error("Tipo Pagamento não é compativel")
	}
	if faturamento.CodigoPrazoDeterminado != arq.Faturamento.CodigoPrazoDeterminado {
		t.Error("Codigo Prazo Determinado não é compativel")
	}
	if faturamento.NumeroDiasPrazoDeterminado != arq.Faturamento.NumeroDiasPrazoDeterminado {
		t.Error("Numero Dias Prazo Determinado não é compativel")
	}
	if faturamento.SemUtilizacao != arq.Faturamento.SemUtilizacao {
		t.Error("Sem Utilizacao não é compativel")
	}

	var dados2 arquivoDePedido.Dados2
	dados2.TipoRegistro = 4
	dados2.NumeroPedido = "2710979"
	dados2.SemUtilizacao = "0000000000000000000"

	if dados2.TipoRegistro != arq.Dados2.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if dados2.NumeroPedido != arq.Dados2.NumeroPedido {
		t.Error("Numero Pedido não é compativel")
	}
	if dados2.SemUtilizacao != arq.Dados2.SemUtilizacao {
		t.Error("Sem Utilizacao não é compativel")
	}

	var dataDoPedido arquivoDePedido.Data
	dataDoPedido.TipoRegistro = 5
	dataDoPedido.Data = 13072017
	dataDoPedido.SemUtilizacao = "00000000000000000000000000"

	if dataDoPedido.TipoRegistro != arq.Data.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if dataDoPedido.Data != arq.Data.Data {
		t.Error("Data não é compativel")
	}
	if dataDoPedido.SemUtilizacao != arq.Data.SemUtilizacao {
		t.Error("Sem Utilizacao não é compativel")
	}

	var horaPedido arquivoDePedido.Hora
	horaPedido.TipoRegistro = 6
	horaPedido.Hora = 113855
	horaPedido.SemUtilizacao = "0000000000000000000000000000"

	if horaPedido.TipoRegistro != arq.Hora.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if horaPedido.Hora != arq.Hora.Hora {
		t.Error("Hora não é compativel")
	}
	if horaPedido.SemUtilizacao != arq.Hora.SemUtilizacao {
		t.Error("Sem Utilizacao não é compativel")
	}

	var itens arquivoDePedido.Itens
	//778975956159470000000300       0600
	itens.TipoRegistro  = int32(7)
	itens.CodigoEanProduto = int64(7897595615947)
	itens.Quantidade = int32(3)
	itens.TipoOcorrencia = int32(00)
	itens.CodigoProduto = ""
	itens.DescontoItem = float32(06.00)

	if itens.TipoRegistro != arq.Itens[1].TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if itens.CodigoEanProduto != arq.Itens[1].CodigoEanProduto {
		t.Error("CodigoEanProduto não é compativel")
	}
	if itens.Quantidade != arq.Itens[1].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if itens.TipoOcorrencia != arq.Itens[1].TipoOcorrencia {
		t.Error("TipoOcorrencia não é compativel")
	}
	if itens.CodigoProduto != arq.Itens[1].CodigoProduto {
		fmt.Println("arquivo codigo", "."+arq.Itens[1].CodigoProduto+"." )
		fmt.Println("CodigoProduto ", "."+itens.CodigoProduto+".")

		t.Error("CodigoProduto não é compativel")
	}
	if itens.DescontoItem != arq.Itens[1].DescontoItem {
		fmt.Println("arquivo DescontoItem", arq.Itens[1].DescontoItem )
		fmt.Println("DescontoItem ", itens.DescontoItem)
		t.Error("DescontoItem não é compativel")
	}

	var Rodape arquivoDePedido.Rodape
	//80000000200000000000000000000000000
	Rodape.TipoRegistro = int32(8)
	Rodape.QuantidadeItens = int32(2)
	Rodape.SemUtilizacao = "00000000000000000000000000"

	if Rodape.TipoRegistro != arq.Rodape.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if Rodape.QuantidadeItens != arq.Rodape.QuantidadeItens {
		t.Error("QuantidadeItens não é compativel")
	}
	if Rodape.SemUtilizacao != arq.Rodape.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

}