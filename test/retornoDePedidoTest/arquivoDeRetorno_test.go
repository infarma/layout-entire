package retornoDePedidoTest

import (
	"fmt"
	"layout-entire/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderRetorno(t *testing.T) {

	data := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)
	hora := time.Date(2000, 2, 1, 12, 30, 45, 2000000000, time.UTC)

	cabecalho := retornoDePedido.GetHeaderRetorno("111111111111111","22222222222222222222", data, hora, 333333333333, "666666666666666")

	if cabecalho != "111111111111111122222222222222222222010220001230470333333333333666666666666666" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}
}

func TestDetalheRetorno(t *testing.T) {

	Detalhe := retornoDePedido.GetDetalheRetorno(1234567891111,"22222222222222222222", "1", 12345, 66666, 333,77777,"99999999999999999999999999999999999999999999999999")

	if Detalhe != "2123456789111122222222222222222222112345666663337777799999999999999999999999999999999999999999999999999" {
		fmt.Println("Detalhe", Detalhe)
		t.Error("Detalhe não é compativel")
	}

}

func TestTrailerRetorno(t *testing.T) {

	Trailer := retornoDePedido.GetTrailerRetorno("22222222222222222222", 11111,22222,33333,44444444444444)

	if Trailer != "32222222222222222222211111222223333344444444444444" {
		fmt.Println("Trailer", Trailer)
		t.Error("Trailer não é compativel")
	}

}