package arquivoDePedidoTest

import (
	"layout-entire/v_3_1/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {
	f, err := os.Open("../fileTest/mock.ped")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var header arquivoDePedido.Cabecalho
	//1222222222222223444444444444455
	header.TipoRegistro = 1
	header.CnpjCliente = "22222222222222"
	header.TipoFaturamento = 3
	header.ApontadorPromocao = "4444444444444"
	header.CodigoControle = "55"
	if header.TipoRegistro != arq.Cabecalho.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if header.CnpjCliente != arq.Cabecalho.CnpjCliente {
		t.Error("CnpjCliente não é compativel")
	}
	if header.TipoFaturamento != arq.Cabecalho.TipoFaturamento {
		t.Error("TipoFaturamento não é compativel")
	}
	if header.ApontadorPromocao != arq.Cabecalho.ApontadorPromocao {
		t.Error("ApontadorPromocao não é compativel")
	}
	if header.CodigoControle != arq.Cabecalho.CodigoControle {
		t.Error("CodigoControle não é compativel")
	}

	var Dados1 arquivoDePedido.Dados1
	Dados1.TipoRegistro = 2
	Dados1.CodigoProjeto = "3"
	Dados1.Pedido = "123456789123456"
	Dados1.CnpjCentroDistribuicao = "12345678912345"
	Dados1.SemUtilizacao = "12345678"

	if Dados1.TipoRegistro != arq.Dados1.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Dados1.CodigoProjeto != arq.Dados1.CodigoProjeto {
		t.Error("CodigoProjeto não é compativel")
	}
	if Dados1.Pedido != arq.Dados1.Pedido {
		t.Error("Pedido não é compativel")
	}
	if Dados1.CnpjCentroDistribuicao != arq.Dados1.CnpjCentroDistribuicao {
		t.Error("CnpjCentroDistribuicao não é compativel")
	}
	if Dados1.SemUtilizacao != arq.Dados1.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

	var Faturamento arquivoDePedido.Faturamento
	Faturamento.TipoRegistro = 3
	Faturamento.TipoPagamento = 4
	Faturamento.CodigoPrazoDeterminado = "1234"
	Faturamento.NumeroDiasPrazoDeterminado = 555
	Faturamento.SemUtilizacao = "0000000000000000000000"

	if Faturamento.TipoRegistro != arq.Faturamento.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Faturamento.TipoPagamento != arq.Faturamento.TipoPagamento {
		t.Error("TipoPagamento não é compativel")
	}
	if Faturamento.CodigoPrazoDeterminado != arq.Faturamento.CodigoPrazoDeterminado {
		t.Error("CodigoPrazoDeterminado não é compativel")
	}
	if Faturamento.NumeroDiasPrazoDeterminado != arq.Faturamento.NumeroDiasPrazoDeterminado {
		t.Error("NumeroDiasPrazoDeterminado não é compativel")
	}
	if Faturamento.SemUtilizacao != arq.Faturamento.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

	var Dados2 arquivoDePedido.Dados2
	Dados2.TipoRegistro = 4
	Dados2.NumeroPedido = "123456789123456"
	Dados2.SemUtilizacao  = "000000000000000"

	if Dados2.TipoRegistro != arq.Dados2.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Dados2.NumeroPedido != arq.Dados2.NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}
	if Dados2.SemUtilizacao != arq.Dados2.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

	var Data arquivoDePedido.Data
	Data.TipoRegistro = 5
	Data.Data = 12345678
	Data.SemUtilizacao = "0000000000000000000000"

	if Data.TipoRegistro != arq.Data.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Data.Data != arq.Data.Data {
		t.Error("Data não é compativel")
	}
	if Data.SemUtilizacao != arq.Data.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

	var Hora arquivoDePedido.Hora
	Hora.TipoRegistro = 6
	Hora.Hora = 123456
	Hora.SemUtilizacao = "000000000000000000000000"

	if Hora.TipoRegistro != arq.Hora.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Hora.Hora != arq.Hora.Hora {
		t.Error("Hora não é compativel")
	}
	if Hora.SemUtilizacao != arq.Hora.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}

	var Itens arquivoDePedido.Itens

	Itens.TipoRegistro = 7
	Itens.CodigoEanProduto = 2222222222222
	Itens.Quantidade = 44444444
	Itens.TipoOcorrencia = 55
	Itens.CodigoProduto = "6666666"
	Itens.DescontoItem  = 77.07

	if Itens.TipoRegistro != arq.Itens[0].TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if Itens.CodigoEanProduto != arq.Itens[0].CodigoEanProduto {
		t.Error("CodigoEanProduto não é compativel")
	}
	if Itens.Quantidade != arq.Itens[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if Itens.TipoOcorrencia != arq.Itens[0].TipoOcorrencia {
		t.Error("TipoOcorrencia não é compativel")
	}
	if Itens.CodigoProduto != arq.Itens[0].CodigoProduto {
		t.Error("CodigoProduto não é compativel")
	}
	if Itens.DescontoItem != arq.Itens[0].DescontoItem {
		t.Error("DescontoItem não é compativel")
	}

	var Rodape arquivoDePedido.Rodape
	Rodape.TipoRegistro = 8
	Rodape.QuantidadeItens = 12345678
	Rodape.SemUtilizacao   = "00000000000000000000000000"

	if Rodape.TipoRegistro != arq.Rodape.TipoRegistro {
		t.Error("Rodape não é compativel")
	}
	if Rodape.QuantidadeItens != arq.Rodape.QuantidadeItens {
		t.Error("QuantidadeItens não é compativel")
	}
	if Rodape.SemUtilizacao != arq.Rodape.SemUtilizacao {
		t.Error("SemUtilizacao não é compativel")
	}




}

