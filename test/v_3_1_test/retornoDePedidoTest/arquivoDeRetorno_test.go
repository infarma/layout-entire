package retornoDePedidoTest

import (
	"fmt"
	"layout-entire/v_3_1/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderRetorno(t *testing.T) {

	cabecalho := retornoDePedido.GetHeaderRetorno("22222222222222","3", "222222222222224")

	if cabecalho != "122222222222222322222222222222400" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}
}

func TestDadosComplementaresRetorno(t *testing.T) {

	data := time.Date(2019, 3, 25, 12, 30, 1, 2, time.UTC)
	hora := time.Date(2000, 2, 1, 12, 30, 45, 0, time.UTC)
	Dados := retornoDePedido.GetDadosComplementaresRetorno(data, hora)

	if Dados != "22503201912304500" {
		fmt.Println("Dados", Dados)
		t.Error("Dados não é compativel")

	}
}

func TestItensPedidoRetorno(t *testing.T) {

	Itens := retornoDePedido.GetItensPedidoRetorno(1234567891234, 12345678,22222222,44,6)

	if Itens != "312345678912341234567822222222446" {
		fmt.Println("Itens", Itens)
		t.Error("Itens não é compativel")

	}
}

func TestFimRetorno(t *testing.T) {

	Fim := retornoDePedido.GetFimRetorno(88888888, 99999999, 33333333)

	if Fim != "4888888889999999933333333000000000000"{
		fmt.Println("Fim", Fim)
		t.Error("Fim não é compativel")

	}
}