package arquivoDeCancelamentoTest

import (
	"fmt"
	"layout-entire/v_3_1/arquivoDeCancelamento"
	"testing"
	"time"
)

func TestGetDistribuidoraCNPJ(t *testing.T) {

	CNPJ := arquivoDeCancelamento.GetDistribuidoraCNPJ("12345671234567","123")

	if CNPJ != "11234567123456712300000000" {
		fmt.Println("CNPJ", CNPJ)
		t.Error("DistribuidoraCNPJ não é compativel")

	}
}

func TestGetClienteCNPJ(t *testing.T) {

	CNPJ := arquivoDeCancelamento.GetClienteCNPJ("12345671234567")

	if CNPJ != "21234567123456700000000000" {
		fmt.Println("CNPJ", CNPJ)
		t.Error("ClienteCNPJ não é compativel")

	}
}

func TestGetPedido(t *testing.T) {

	Pedido := arquivoDeCancelamento.GetPedido(123456781234567)

	if Pedido != "31234567812345670000000000" {
		fmt.Println("Pedido", Pedido)
		t.Error("Pedido não é compativel")

	}
}

func TestGetItem(t *testing.T) {

	Item := arquivoDeCancelamento.GetItem(123456781234567,1234567891,12)

	if Item != "4123456781234567123456789112" {
		fmt.Println("Item", Item)
		t.Error("Item não é compativel")

	}
}

func TestGetDadosComplementares(t *testing.T) {

	data := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)
	hora := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)
	Dados := arquivoDeCancelamento.GetDadosComplementares(data,hora)

	if Dados != "501022000123001" {
		fmt.Println("Dados", Dados)
		t.Error("Dados não é compativel")

	}
}