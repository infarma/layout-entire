package arquivoDeNotaFiscalTest

import (
	"fmt"
	"layout-entire/v_3_1/arquivoDeNotaFiscal"
	"testing"
	"time"
)

func TestHeaderNota(t *testing.T) {

	data := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)
	hora := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)

	cabecalho := arquivoDeNotaFiscal.GetCabecalho(data,hora,12345678999999, "1",7777777,"123456789111111")

	if cabecalho != "10102200012300112345678999999177777771234567891111110000000000000000000000000000" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}

}

func TestDadosNota(t *testing.T) {

	data := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)
	hora := time.Date(2000, 2, 1, 12, 30, 1, 2, time.UTC)

	Dados := arquivoDeNotaFiscal.GetDados(data,hora,data,"12345678911111",123456,"111",88888888)

	if Dados != "20102200012300101022000123456789111111234561118888888800000000000000000000000000" {
		fmt.Println("Dados", Dados)
		t.Error("Dados não é compativel")

	}

}

func TestTotaisNota(t *testing.T) {

	Totais := arquivoDeNotaFiscal.GetTotais(666666.22,666666.22,666666.22,666666.22,666666.22,666666.22)

	if Totais != "36666662266666622666666226666662266666622666666220000000000000000000000000000000" {
		fmt.Println("Totais", Totais)
		t.Error("Totais não é compativel")

	}

}

func TestICSM(t *testing.T) {

	ICSM := arquivoDeNotaFiscal.GetICSM(666666.22, 666666.22, 666666.22, 666666.22)

	if ICSM != "46666662266666622666666226666662200000000000000000000000000000000000000000000000" {
		fmt.Println("ICSM", ICSM)
		t.Error("ICSM não é compativel")

	}

}

func TestItens(t *testing.T) {

	tam8 := float64(666666.22)
	tam3 := float64(1.22)
	tam4 := float64(33.44)
	Itens := arquivoDeNotaFiscal.GetItens("1234567891111", "1234567", 4444, tam3,tam8,tam4,tam8,tam8,tam4,tam8,tam4 )

	if Itens != "51234567891111123456744441226666662233446666662266666622334466666622334400000000" {
		fmt.Println("Itens", Itens)
		t.Error("Itens não é compativel")

	}

}

func TestFim(t *testing.T) {

	Fim := arquivoDeNotaFiscal.GetFim(4444)

	if Fim != "64444000000000000000000000000000000000000000000000000000000000000000000000000000" {
		fmt.Println("Fim", Fim)
		t.Error("Fim não é compativel")

	}

}
