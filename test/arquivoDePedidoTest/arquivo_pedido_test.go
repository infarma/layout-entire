package arquivoDePedidoTest

import (
	"layout-entire/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {
	f, err := os.Open("../fileTest/PEDEUR_12345678912345.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var header arquivoDePedido.Header
	//122222222222222233333333333333333333444444445677777777777777788888999
	header.TipoRegistro = 1
	header.CodigoCliente = "222222222222222"
	header.NumeroPedido = "33333333333333333333"
	header.DataPedido = 44444444
	header.TipoCompra = "5"
	header.TipoRetorno = "6"
	header.CnpjOLFaturamento = "777777777777777"
	header.ApontadorCondicaoComercial = 88888
	header.PrazoApontadorComercial = "999"

	if header.TipoRegistro != arq.Header.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if header.CodigoCliente != arq.Header.CodigoCliente {
		t.Error("CodigoCliente não é compativel")
	}
	if header.NumeroPedido != arq.Header.NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}
	if header.DataPedido != arq.Header.DataPedido {
		t.Error("DataPedido não é compativel")
	}
	if header.TipoCompra != arq.Header.TipoCompra {
		t.Error("TipoCompra não é compativel")
	}
	if header.TipoRetorno != arq.Header.TipoRetorno {
		t.Error("TipoRetorno não é compativel")
	}
	if header.CnpjOLFaturamento != arq.Header.CnpjOLFaturamento {
		t.Error("CnpjOLFaturamento não é compativel")
	}
	if header.ApontadorCondicaoComercial != arq.Header.ApontadorCondicaoComercial {
		t.Error("ApontadorCondicaoComercial não é compativel")
	}
	if header.PrazoApontadorComercial != arq.Header.PrazoApontadorComercial {
		t.Error("PrazoApontadorComercial não é compativel")
	}

	var detalhe arquivoDePedido.Detalhe
	//233333333333333333333444444444444455555666667777788891
	detalhe.TipoRegistro = 2
	detalhe.NumeroPedido = "33333333333333333333"
	detalhe.CodigoProduto = 4444444444444
	detalhe.Quantidade = 55555
	detalhe.ApontadorCondicaoComercial = 66666
	detalhe.Desconto = 77777
	detalhe.Prazo = 888
	detalhe.UtilizacaoDesconto = "9"
	detalhe.UtilizacaoPrazo = "1"


	if detalhe.TipoRegistro != arq.Detalhe[0].TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}

	if detalhe.NumeroPedido != arq.Detalhe[0].NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}

	if detalhe.CodigoProduto != arq.Detalhe[0].CodigoProduto {
		t.Error("CodigoProduto não é compativel")
	}

	if detalhe.Quantidade != arq.Detalhe[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}

	if detalhe.ApontadorCondicaoComercial != arq.Detalhe[0].ApontadorCondicaoComercial {
		t.Error("ApontadorCondicaoComercial não é compativel")
	}

	if detalhe.Desconto != arq.Detalhe[0].Desconto {
		t.Error("Desconto não é compativel")
	}

	if detalhe.Prazo != arq.Detalhe[0].Prazo {
		t.Error("Prazo não é compativel")
	}

	if detalhe.UtilizacaoDesconto != arq.Detalhe[0].UtilizacaoDesconto {
		t.Error("UtilizacaoDesconto não é compativel")
	}

	if detalhe.UtilizacaoPrazo != arq.Detalhe[0].UtilizacaoPrazo {
		t.Error("UtilizacaoPrazo não é compativel")
	}

	var Trailer arquivoDePedido.Trailler
	//344444444444444444444555556666666666
	Trailer.TipoRegistro = 3
	Trailer.NumeroPedido = "44444444444444444444"
	Trailer.QuantidadeItens = 55555
	Trailer.QuantidadeUnidades = 6666666666

	if Trailer.TipoRegistro != arq.Trailler.TipoRegistro {
		t.Error("Tipo Registro não é compativel")
	}
	if Trailer.NumeroPedido != arq.Trailler.NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}
	if Trailer.QuantidadeItens != arq.Trailler.QuantidadeItens {
		t.Error("QuantidadeItens não é compativel")
	}
	if Trailer.QuantidadeUnidades != arq.Trailler.QuantidadeUnidades {
		t.Error("QuantidadeUnidades não é compativel")
	}

}

