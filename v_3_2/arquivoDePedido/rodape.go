package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Rodape struct {
	TipoRegistro    int32  `json:"TipoRegistro"`
	QuantidadeItens int32  `json:"QuantidadeItens"`
	SemUtilizacao   string `json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeItens, "QuantidadeItens")
	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")

	r.SemUtilizacao = strings.TrimSpace(r.SemUtilizacao)

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":    {0, 1, 0},
	"QuantidadeItens": {1, 9, 0},
	"SemUtilizacao":   {9, 35, 0},
}
