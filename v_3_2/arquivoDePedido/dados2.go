package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Dados2 struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	NumeroPedido  string `json:"NumeroPedido"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (d *Dados2) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados2

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")

	d.NumeroPedido = strings.TrimSpace(d.NumeroPedido)
	d.SemUtilizacao = strings.TrimSpace(d.SemUtilizacao)

	return err
}

var PosicoesDados2 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"NumeroPedido":  {1, 16, 0},
	"SemUtilizacao": {16, 35, 0},
}
