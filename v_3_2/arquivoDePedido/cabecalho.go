package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Cabecalho struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	CnpjCliente        string `json:"CnpjCliente"`
	TipoFaturamento    int32  `json:"TipoFaturamento"`
	ApontadorPromoccao string `json:"ApontadorPromoccao"`
	CodigoControle     string `json:"CodigoControle"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	err = posicaoParaValor.ReturnByType(&c.TipoFaturamento, "TipoFaturamento")
	err = posicaoParaValor.ReturnByType(&c.ApontadorPromoccao, "ApontadorPromoccao")
	err = posicaoParaValor.ReturnByType(&c.CodigoControle, "CodigoControle")

	c.ApontadorPromoccao = strings.TrimSpace(c.ApontadorPromoccao)
	c.CodigoControle = strings.TrimSpace(c.CodigoControle)

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"CnpjCliente":        {1, 15, 0},
	"TipoFaturamento":    {15, 16, 0},
	"ApontadorPromoccao": {16, 29, 0},
	"CodigoControle":     {29, 35, 0},
}
