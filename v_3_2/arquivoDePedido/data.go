package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Data struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	Data          int32  `json:"Data"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (d *Data) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesData

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&d.Data, "Data")
	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")

	d.SemUtilizacao =strings.TrimSpace(d.SemUtilizacao)

	return err
}

var PosicoesData = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"Data":          {1, 9, 0},
	"SemUtilizacao": {9, 35, 0},
}
