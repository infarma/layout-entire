package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Dados1 struct {
	TipoRegistro           int32  `json:"TipoRegistro"`
	CodigoProjeto          string `json:"CodigoProjeto"`
	Pedido                 string `json:"Pedido"`
	CnpjCentroDistribuicao string `json:"CnpjCentroDistribuicao"`
	SemUtilizacao          string `json:"SemUtilizacao"`
}

func (d *Dados1) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados1

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&d.CodigoProjeto, "CodigoProjeto")
	err = posicaoParaValor.ReturnByType(&d.Pedido, "Pedido")
	err = posicaoParaValor.ReturnByType(&d.CnpjCentroDistribuicao, "CnpjCentroDistribuicao")
	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")

	d.CodigoProjeto = strings.TrimSpace(d.CodigoProjeto)
	d.Pedido = strings.TrimSpace(d.Pedido)
	d.CnpjCentroDistribuicao = strings.TrimSpace(d.CnpjCentroDistribuicao)
	d.SemUtilizacao = strings.TrimSpace(d.SemUtilizacao)

	return err
}

var PosicoesDados1 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":           {0, 1, 0},
	"CodigoProjeto":          {1, 2, 0},
	"Pedido":                 {2, 17, 0},
	"CnpjCentroDistribuicao": {17, 31, 0},
	"SemUtilizacao":          {31, 35, 0},
}
