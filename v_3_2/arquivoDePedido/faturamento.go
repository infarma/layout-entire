package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Faturamento struct {
	TipoRegistro               int32  `json:"TipoRegistro"`
	TipoPagamento              int32  `json:"TipoPagamento"`
	CodigoPrazoDeterminado     string `json:"CodigoPrazoDeterminado"`
	NumeroDiasPrazoDeterminado int32  `json:"NumeroDiasPrazoDeterminado"`
	SemUtilizacao              string `json:"SemUtilizacao"`
}

func (f *Faturamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesFaturamento

	err = posicaoParaValor.ReturnByType(&f.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&f.TipoPagamento, "TipoPagamento")
	err = posicaoParaValor.ReturnByType(&f.CodigoPrazoDeterminado, "CodigoPrazoDeterminado")
	err = posicaoParaValor.ReturnByType(&f.NumeroDiasPrazoDeterminado, "NumeroDiasPrazoDeterminado")
	err = posicaoParaValor.ReturnByType(&f.SemUtilizacao, "SemUtilizacao")

	f.CodigoPrazoDeterminado = strings.TrimSpace(f.CodigoPrazoDeterminado)
	f.SemUtilizacao = strings.TrimSpace(f.SemUtilizacao)

	return err
}

var PosicoesFaturamento = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 1, 0},
	"TipoPagamento":              {1, 2, 0},
	"CodigoPrazoDeterminado":     {2, 6, 0},
	"NumeroDiasPrazoDeterminado": {6, 9, 0},
	"SemUtilizacao":              {9, 35, 0},
}
