package arquivoDePedido

import (
	"bitbucket.org/infarma/gerador-layouts-posicoes"
	"strings"
)

type Itens struct {
	TipoRegistro     int32   `json:"TipoRegistro"`
	CodigoEanProduto int64   `json:"CodigoEanProduto"`
	Quantidade       int32   `json:"Quantidade"`
	TipoOcorrencia   int32   `json:"TipoOcorrencia"`
	CodigoProduto    string  `json:"CodigoProduto"`
	DescontoItem     float32 `json:"DescontoItem"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	err = posicaoParaValor.ReturnByType(&i.CodigoEanProduto, "CodigoEanProduto")
	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	err = posicaoParaValor.ReturnByType(&i.TipoOcorrencia, "TipoOcorrencia")
	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	err = posicaoParaValor.ReturnByType(&i.DescontoItem, "DescontoItem")

	i.CodigoProduto = strings.TrimSpace(i.CodigoProduto)

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":     {0, 1, 0},
	"CodigoEanProduto": {1, 14, 0},
	"Quantidade":       {14, 22, 0},
	"TipoOcorrencia":   {22, 24, 0},
	"CodigoProduto":    {24, 31, 0},
	"DescontoItem":     {31, 35, 2},
}
