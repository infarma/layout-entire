package retornoDePedido

import (
	"fmt"
	"time"
)

func GetCabecalho(cnpjCliente string, codProjeto string, pedidoLab string) string{


	cabecalho := "1"
	cabecalho += fmt.Sprintf("%-14s", cnpjCliente)
	cabecalho += fmt.Sprintf("%-1s", codProjeto)
	cabecalho += fmt.Sprintf("%-15s", pedidoLab)
	cabecalho += fmt.Sprintf("%02s", "")

	return cabecalho
}

func GetDados(data time.Time, hora time.Time) string{


	cabecalho := "2"
	cabecalho += data.Format("02012006")
	cabecalho += hora.Format("150405")
	cabecalho += fmt.Sprintf("%018s", "")

	return cabecalho
}

func GetItens(ean string, qtdAtendida int64, qtdNaoAtendida int64, motivo int32, retorno int8) string{

	cabecalho := "3"
	cabecalho += fmt.Sprintf("%-13s",ean)
	cabecalho += fmt.Sprintf("%08d", qtdAtendida)
	cabecalho += fmt.Sprintf("%08d", qtdNaoAtendida)
	cabecalho += fmt.Sprintf("%02d", motivo)
	cabecalho += fmt.Sprintf("%01d", retorno)

	return cabecalho
}

func GetFim(qtdAtendida int64, qtdNaoAtendida int64, qtdItensArquivo int64) string{

	cabecalho := "4"
	cabecalho += fmt.Sprintf("%08d", qtdAtendida)
	cabecalho += fmt.Sprintf("%08d", qtdNaoAtendida)
	cabecalho += fmt.Sprintf("%08d", qtdItensArquivo)
	cabecalho += fmt.Sprintf("%08s", "")

	return cabecalho
}