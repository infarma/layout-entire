package arquivoDeCancelamento

import (
	"fmt"
	"time"
)

func GetDistribuidoraCNPJ (CnpjCD string, TipoAcao string) string{

	CNPJ := "1"
	CNPJ += fmt.Sprintf("%014s", CnpjCD)
	CNPJ += fmt.Sprintf("%03s", TipoAcao)
	CNPJ += fmt.Sprintf("%08s", "")

	return CNPJ

}

func GetClienteCNPJ (CnpjCliente string) string{

	CNPJ := "2"
	CNPJ += fmt.Sprintf("%014s", CnpjCliente)
	CNPJ += fmt.Sprintf("%011s", "")

	return CNPJ

}

func GetPedido (NumeroPedido int64) string{

	Pedido := "3"
	Pedido += fmt.Sprintf("%015d", NumeroPedido)
	Pedido += fmt.Sprintf("%010s", "")

	return Pedido

}

func GetItem (EanProduto int64, Quantidade int32, MotivoCancelamento int32   ) string{

	Item := "4"
	Item += fmt.Sprintf("%015d", EanProduto)
	Item += fmt.Sprintf("%08d", Quantidade)
	Item += fmt.Sprintf("%02d", MotivoCancelamento)

	return Item

}

func GetDadosComplementares ( Data time.Time, Hora time.Time) string{

	Dados := "5"
	Dados += Data.Format("02012006")
	Dados += Hora.Format("150405")

	return Dados

}