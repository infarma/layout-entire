package arquivoDeCancelamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CnpjCliente struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	CnpjCliente   string `json:"CnpjCliente"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (c *CnpjCliente) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCnpjCliente

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCnpjCliente = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"CnpjCliente":   {1, 15, 0},
	"SemUtilizacao": {15, 26, 0},
}
