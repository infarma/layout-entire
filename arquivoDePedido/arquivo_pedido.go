package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Header   Header    `json:"Header"`
	Detalhe  []Detalhe `json:"Detalhe"`
	Trailler Trailler  `json:"Trailler"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		if identificador == "1" {
			err = arquivo.Header.ComposeStruct(string(runes))
		} else if identificador == "2" {
			Detalhe := Detalhe{}
			Detalhe.ComposeStruct(string(runes))
			arquivo.Detalhe = append(arquivo.Detalhe, Detalhe)
		} else if identificador == "3" {
			err = arquivo.Trailler.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
