package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	TipoRegistro               int32  `json:"TipoRegistro"`
	NumeroPedido               string `json:"NumeroPedido"`
	CodigoProduto              int64  `json:"CodigoProduto"`
	Quantidade                 int32  `json:"Quantidade"`
	ApontadorCondicaoComercial int32  `json:"ApontadorCondicaoComercial"`
	Desconto                   int32  `json:"Desconto"`
	Prazo                      int32  `json:"Prazo"`
	UtilizacaoDesconto         string `json:"UtilizacaoDesconto"`
	UtilizacaoPrazo            string `json:"UtilizacaoPrazo"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ApontadorCondicaoComercial, "ApontadorCondicaoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Desconto, "Desconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Prazo, "Prazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.UtilizacaoDesconto, "UtilizacaoDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.UtilizacaoPrazo, "UtilizacaoPrazo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 1, 0},
	"NumeroPedido":               {1, 21, 0},
	"CodigoProduto":              {21, 34, 0},
	"Quantidade":                 {34, 39, 0},
	"ApontadorCondicaoComercial": {39, 44, 0},
	"Desconto":                   {44, 49, 0},
	"Prazo":                      {49, 52, 0},
	"UtilizacaoDesconto":         {52, 53, 0},
	"UtilizacaoPrazo":            {53, 54, 0},
}
