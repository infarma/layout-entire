package layout_entire

import (
	"bitbucket.org/infarma/layout-entire/arquivoDePedido"
	arquivoDePedido31 "bitbucket.org/infarma/layout-entire/v_3_1/arquivoDePedido"
	arquivoDePedido32 "bitbucket.org/infarma/layout-entire/v_3_2/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}

func GetArquivoDePedido31(fileHandle *os.File) (arquivoDePedido31.ArquivoDePedido, error) {
	return arquivoDePedido31.GetStruct(fileHandle)
}

func GetArquivoDePedido32(fileHandle *os.File) (arquivoDePedido32.ArquivoDePedido, error) {
	return arquivoDePedido32.GetStruct(fileHandle)
}
