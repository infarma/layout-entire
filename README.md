### Entire v_3_1
## Arquivo de Pedidos
gerador-layouts arquivoDePedido cabecalho TipoRegistro:int32:0:1 CnpjCliente:string:1:15 TipoFaturamento:int32:15:16 ApontadorPromocao:string:16:29 CodigoControle:string:29:31

gerador-layouts arquivoDePedido dados1 TipoRegistro:int32:0:1 CodigoProjeto:string:1:2 Pedido:string:2:17 CnpjCentroDistribuicao:string:17:31 SemUtilizacao:string:31:39

gerador-layouts arquivoDePedido faturamento TipoRegistro:int32:0:1 TipoPagamento:int32:1:2 CodigoPrazoDeterminado:string:2:6 NumeroDiasPrazoDeterminado:int32:6:9 SemUtilizacao:string:9:31

gerador-layouts arquivoDePedido dados2 TipoRegistro:int32:0:1 NumeroPedido:string:1:16 SemUtilizacao:string:16:31

gerador-layouts arquivoDePedido data TipoRegistro:int32:0:1 Data:int32:1:9 SemUtilizacao:string:9:31

gerador-layouts arquivoDePedido hora TipoRegistro:int32:0:1 Hora:int32:1:7 SemUtilizacao:string:7:31

gerador-layouts arquivoDePedido itens TipoRegistro:int32:0:1 CodigoEanProduto:int64:1:14 Quantidade:int32:14:22 TipoOcorrencia:int32:22:24 CodigoProduto:string:24:31 DescontoItem:float32:31:35:2

gerador-layouts arquivoDePedido rodape TipoRegistro:int32:0:1 QuantidadeItens:int32:1:9 SemUtilizacao:string:9:35


## Arquivo de Retorno
gerador-layouts retornoDePedido cabecalho TipoRegistro:int32:0:1 CnpjCliente:string:1:15 CodigoProjeto:string:15:16 PedidoLaboratorio:string:16:31 SemUtilizacao:string:31:33

gerador-layouts retornoDePedido dados TipoRegistro:int32:0:1 Data:int32:1:9 Hora:int32:9:15 SemUtilizacao:string:15:25

gerador-layouts retornoDePedido itens TipoRegistro:int32:0:1 EanProduto:int64:1:14 QuantidadeAtendida:int32:14:22 QuantidadeNaoAtendida:int32:22:30 Motivo:int32:30:32 Retorno:int32:32:33

gerador-layouts retornoDePedido rodape TipoRegistro:int32:0:1 QuantidadeUnidadesAtendidas:int32:1:9 QuantidadeUnidadesNaoAtendidas:int32:9:18 QuantidadeItensArquivo:int32:18:26 SemUtilizacao:string:26:37


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal cabecalho TipoRegistro:int32:0:1 DataGeracaoArquivo:int32:1:9 HoraGeracaoArquivo:int32:9:15 CnpjOperador:int64:15:29 CodigoProjeto:string:29:30 NumeroPedidoLaboratorio:int32:30:37 NumeroPedidoCliente:string:37:52 Livre:string:52:80

gerador-layouts arquivoDeNotaFiscal dados TipoRegistro:int32:0:1 DataSaidaMercadoria:int32:1:9 HoraSaidaMercadoria:int32:9:15 DataEmissaoNotaFiscal:int32:15:23 CnpjLoja:string:23:37 NumeroNotaFiscal:int32:37:43 SerieDocumento:string:43:46 VencimentoNotaFiscal:int32:46:54 Livre:string:54:80

gerador-layouts arquivoDeNotaFiscal totais TipoRegistro:int32:0:1 ValorFrete:float32:1:9:2 ValorSeguro:float32:9:17:2 OutrasDespesas:float32:17:25:2 ValorTotalProdutos:float32:25:33:2 ValorTotalNF:float32:33:41:2 ValorIPI:float32:41:49:2 Livre:string:49:80

gerador-layouts arquivoDeNotaFiscal icms TipoRegistro:int32:0:1 BaseCalculoICMS:float32:1:9:2 ValorICMS:float32:9:17:2 BaseCalculoSubstituicaoTributaria:float32:17:25:2 ValorICMSSubstituicaoTributaria:float32:25:33:2 Livre:string:33:80

gerador-layouts arquivoDeNotaFiscal itens TipoRegistro:int32:0:1 CodigoEan:string:1:14 CodigoProdutoOperador:string:14:21 Quantidade:int32:21:25 Unidade:string:25:27 PrecoFabrica:float32:27:35:2 DescontoComercial:int32:35:39:2 ValorDescontoComercial:float32:39:47:2 ValorRepasse:float32:47:55:2 Repasse:float32:55:59:2 ValorUnitario:float32:59:67:2 Fracionamento:float32:67:71:2 Livre:string:71:80

gerador-layouts arquivoDeNotaFiscal rodape TipoRegistro:int32:0:1 NumeroItensNotaFiscal:int32:1:5 Livre:string:5:80



### Entire v_3_2
## Arquivo de Pedido
gerador-layouts arquivoDePedido cabecalho TipoRegistro:int32:0:1 CnpjCliente:string:1:15 TipoFaturamento:int32:15:16 ApontadorPromoccao:string:16:29 CodigoControle:string:29:35

gerador-layouts arquivoDePedido dados1 TipoRegistro:int32:0:1 CodigoProjeto:string:1:2 Pedido:string:2:17 CnpjCentroDistribuicao:string:17:31 SemUtilizacao:string:31:35

gerador-layouts arquivoDePedido faturamento TipoRegistro:int32:0:1 TipoPagamento:int32:1:2 CodigoPrazoDeterminado:string:2:6 NumeroDiasPrazoDeterminado:int32:6:9 SemUtilizacao:string:9:35

gerador-layouts arquivoDePedido data TipoRegistro:int32:0:1 Data:int32:1:9 SemUtilizacao:string:9:35

gerador-layouts arquivoDePedido hora TipoRegistro:int32:0:1 Hora:int32:1:7 SemUtilizacao:string:7:35

gerador-layouts arquivoDePedido itens TipoRegistro:int32:0:1 CodigoEanProduto:int64:1:14 Quantidade:int32:14:22 TipoOcorrencia:int32:22:24 CodigoProduto:string:24:31 DescontoItem:float32:31:35:2

gerador-layouts arquivoDePedido rodape TipoRegistro:int32:0:1 QuantidadeItens:int32:1:9 SemUtilizacao:string:9:35


## Arquivo de Retorno
gerador-layouts retornoDePedido cabecalho TipoRegistro:int32:0:1 CnpjCliente:string:1:15 CodigoProjeto:string:15:16 PedidoLaboratorio:string:16:31 SemUtilizacao:string:31:33

gerador-layouts retornoDePedido dados TipoRegistro:int32:0:1 Data:int32:1:9 Hora:int32:9:15 SemUtilizacao:string:15:33

gerador-layouts retornoDePedido itens TipoRegistro:int32:0:1 EanProduto:int64:1:14 QuantidadeAtendida:int32:14:22 QuantidadeNaoAtendida:int32:22:30 Motivo:int32:30:32 Retorno:int32:32:33

gerador-layouts retornoDePedido rodape TipoRegistro:int32:0:1 QuantidadeUnidadesAtendidas:int32:1:9 QuantidadeUnidadesNaoAtendidas:int32:9:17 QuantidadeItensArquivo:int32:17:25 SemUtilizacao:string:25:33


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal cabecalho TipoRegistro:int32:0:1 DataGeracaoArquivo:int32:1:9 HoraGeracaoArquivo:int32:9:15 CnpjOperador:int64:15:29 CodigoProjeto:string:29:30 NumeroPedidoLaboratorio:int32:30:37 NumeroPedidoCliente:string:37:52 Livre:string:52:80

gerador-layouts arquivoDeNotaFiscal dados TipoRegistro:int32:0:1 DataSaidaMercadoria:int32:1:9 HoraSaidaMercadoria:int32:9:15 DataEmissaoNotaFiscal:int32:15:23 CnpjLoja:string:23:37 NumeroNotaFiscal:int32:37:43 SerieDocumento:string:43:46 VencimentoNotaFiscal:int32:46:54 Livre:string:54:80

gerador-layouts arquivoDeNotaFiscal totais TipoRegistro:int32:0:1 ValorFrete:float32:1:9:2 ValorSeguro:float32:9:17:2 OutrasDespesas:float32:17:25:2 ValorTotalProdutos:float32:25:33:2 ValorTotalNF:float32:33:41:2 ValorIPI:float32:41:49:2 Livre:string:49:80

gerador-layouts arquivoDeNotaFiscal icms TipoRegistro:int32:0:1 BaseCalculoICMS:float32:1:9:2 ValorICMS:float32:9:17:2 BaseCalculoSubstituicaoTributaria:float32:17:25:2 ValorICMSSubstituicaoTributaria:float32:25:33:2 Livre:string:33:80

gerador-layouts arquivoDeNotaFiscal itens TipoRegistro:int32:0:1 CodigoEan:string:1:14 CodigoProdutoOperador:string:14:21 Quantidade:int32:21:25 Unidade:string:25:27 PrecoFabrica:float32:27:35:2 DescontoComercial:int32:35:39:2 ValorDescontoComercial:float32:39:47:2 ValorRepasse:float32:47:55:2 Repasse:float32:55:59:2 ValorUnitario:float32:59:67:2 Fracionamento:float32:67:71:2 Livre:string:71:80

gerador-layouts arquivoDeNotaFiscal rodape TipoRegistro:int32:0:1 NumeroItensNotaFiscal:int32:1:5 Livre:string:5:80


## Arquivo de Cancelamento
gerador-layouts arquivoDeCancelamento CnpjDistribuidora TipoRegistro:int32:0:1 CnpjCD:string:1:15 TipoAcao:string:15:18 SemUtilizacao:string:18:26

gerador-layouts arquivoDeCancelamento CnpjCliente TipoRegistro:int32:0:1 CnpjCliente:string:1:15 SemUtilizacao:string:15:26

gerador-layouts arquivoDeCancelamento pedido TipoRegistro:int32:0:1 NumeroPedido:int32:1:16 SemUtilizacao:string:16:26

gerador-layouts arquivoDeCancelamento item TipoRegistro:int32:0:1 EanProduto:int32:1:16 Quantidade:int32:16:24 MotivoCancelamento:int32:24:26

gerador-layouts arquivoDeCancelamento dados TipoRegistro:int32:0:1 Data:int32:1:9 Hora:int32:9:15 SemUtilizacao:string:15:26