package retornoDePedido

import (
	"fmt"
	"time"
)

func GetHeaderRetorno(	CnpjFarmacia string, NumeroPedidoIndustria string, DataProcessamento time.Time, HoraProcessamento time.Time, NumeroPedidoOL int64, CnpjDistribuidorFaturamento string) string{


	cabecalho := "1"
	cabecalho += fmt.Sprintf("%-15s", CnpjFarmacia)
	cabecalho += fmt.Sprintf("%-20s", NumeroPedidoIndustria)
	cabecalho += DataProcessamento.Format("02012006")
	cabecalho += HoraProcessamento.Format("1504050")
	cabecalho += fmt.Sprintf("%012d", NumeroPedidoOL)
	cabecalho += fmt.Sprintf("%-15s", CnpjDistribuidorFaturamento )

	return cabecalho
}

func GetDetalheRetorno(	CodigoProduto int64, NumeroPedido string, CondicaoPagamento string, QuantidadeAtendida int32, DescontoAplicado int32, PrazoConcedido int32, QuantidadeNaoAtendida int32, Motivo string) string{


	Detalhe := "2"
	Detalhe += fmt.Sprintf("%013d", CodigoProduto)
	Detalhe += fmt.Sprintf("%-20s", NumeroPedido)
	Detalhe += fmt.Sprintf("%-1s", CondicaoPagamento)
	Detalhe += fmt.Sprintf("%05d", QuantidadeAtendida)
	Detalhe += fmt.Sprintf("%05d", DescontoAplicado)
	Detalhe += fmt.Sprintf("%03d", PrazoConcedido)
	Detalhe += fmt.Sprintf("%05d", QuantidadeNaoAtendida)
	Detalhe += fmt.Sprintf("%-50s", Motivo)

	return Detalhe
}

func GetTrailerRetorno(	NumeroPedido string, QuantidadeLinhas int32, QuantidadeItensAtendidos int32, QuantidaeItensNaoAtendida int32, LimiteDisponivel int64) string{


	Trailer := "3"
	Trailer += fmt.Sprintf("%-20s", NumeroPedido)
	Trailer += fmt.Sprintf("%05d", QuantidadeLinhas)
	Trailer += fmt.Sprintf("%05d", QuantidadeItensAtendidos)
	Trailer += fmt.Sprintf("%05d", QuantidaeItensNaoAtendida)
	Trailer += fmt.Sprintf("%014d", LimiteDisponivel)

	return Trailer
}