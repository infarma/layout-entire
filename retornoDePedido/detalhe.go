package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	TipoRegistro          int32   `json:"TipoRegistro"`
	CodigoProduto         int64   `json:"CodigoProduto"`
	NumeroPedido          string  `json:"NumeroPedido"`
	CondicaoPagamento     string  `json:"CondicaoPagamento"`
	QuantidadeAtendida    int32   `json:"QuantidadeAtendida"`
	DescontoAplicado      int32 `json:"DescontoAplicado"`
	PrazoConcedido        int32   `json:"PrazoConcedido"`
	QuantidadeNaoAtendida int32   `json:"QuantidadeNaoAtendida"`
	Motivo                string  `json:"Motivo"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CondicaoPagamento, "CondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoAplicado, "DescontoAplicado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PrazoConcedido, "PrazoConcedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadeNaoAtendida, "QuantidadeNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Motivo, "Motivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":          {0, 1, 0},
	"CodigoProduto":         {1, 14, 0},
	"NumeroPedido":          {14, 34, 0},
	"CondicaoPagamento":     {34, 35, 0},
	"QuantidadeAtendida":    {35, 40, 0},
	"DescontoAplicado":      {40, 45, 2},
	"PrazoConcedido":        {45, 48, 0},
	"QuantidadeNaoAtendida": {48, 53, 0},
	"Motivo":                {53, 103, 0},
}
