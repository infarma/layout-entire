package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	TipoRegistro              int32  	`json:"TipoRegistro"`
	NumeroPedido              string 	`json:"NumeroPedido"`
	QuantidadeLinhas          int32  	`json:"QuantidadeLinhas"`
	QuantidadeItensAtendidos  int32  	`json:"QuantidadeItensAtendidos"`
	QuantidaeItensNaoAtendida int32  	`json:"QuantidaeItensNaoAtendida"`
	LimiteDisponivel          float64	`json:"LimiteDisponivel"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeLinhas, "QuantidadeLinhas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeItensAtendidos, "QuantidadeItensAtendidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidaeItensNaoAtendida, "QuantidaeItensNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.LimiteDisponivel, "LimiteDisponivel")
	if err != nil {
		return err
	}


	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"NumeroPedido":                      {1, 21, 0},
	"QuantidadeLinhas":                      {21, 26, 0},
	"QuantidadeItensAtendidos":                      {26, 31, 0},
	"QuantidaeItensNaoAtendida":                      {31, 36, 0},
	"LimiteDisponivel":                      {36, 50, 2},
}