package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	TipoRegistro                int32 	`json:"TipoRegistro"`
	CnpjFarmacia                string	`json:"CnpjFarmacia"`
	NumeroPedidoIndustria       string	`json:"NumeroPedidoIndustria"`
	DataProcessamento           int32 	`json:"DataProcessamento"`
	HoraProcessamento           int32 	`json:"HoraProcessamento"`
	NumeroPedidoOL              int64 	`json:"NumeroPedidoOL"`
	CnpjDistribuidorFaturamento string	`json:"CnpjDistribuidorFaturamento"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjFarmacia, "CnpjFarmacia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoIndustria, "NumeroPedidoIndustria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataProcessamento, "DataProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraProcessamento, "HoraProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoOL, "NumeroPedidoOL")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CnpjDistribuidorFaturamento, "CnpjDistribuidorFaturamento")
	if err != nil {
		return err
	}


	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CnpjFarmacia":                      {1, 16, 0},
	"NumeroPedidoIndustria":                      {16, 36, 0},
	"DataProcessamento":                      {36, 44, 0},
	"HoraProcessamento":                      {44, 51, 0},
	"NumeroPedidoOL":                      {51, 63, 0},
	"CnpjDistribuidorFaturamento":                      {63, 78, 0},
}