package arquivoDeCancelamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CnpjDistribuidora struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	CnpjCD        string `json:"CnpjCD"`
	TipoAcao      string `json:"TipoAcao"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (c *CnpjDistribuidora) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCnpjDistribuidora

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCD, "CnpjCD")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoAcao, "TipoAcao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCnpjDistribuidora = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"CnpjCD":        {1, 15, 0},
	"TipoAcao":      {15, 18, 0},
	"SemUtilizacao": {18, 26, 0},
}
