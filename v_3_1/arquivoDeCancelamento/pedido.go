package arquivoDeCancelamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Pedido struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	NumeroPedido  int32  `json:"NumeroPedido"`
	SemUtilizacao string `json:"SemUtilizacao"`
}

func (p *Pedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPedido

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"NumeroPedido":  {1, 16, 0},
	"SemUtilizacao": {16, 26, 0},
}
