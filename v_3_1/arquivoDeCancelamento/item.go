package arquivoDeCancelamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Item struct {
	TipoRegistro       int32 `json:"TipoRegistro"`
	EanProduto         int32 `json:"EanProduto"`
	Quantidade         int32 `json:"Quantidade"`
	MotivoCancelamento int32 `json:"MotivoCancelamento"`
}

func (i *Item) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItem

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.EanProduto, "EanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.MotivoCancelamento, "MotivoCancelamento")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItem = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"EanProduto":         {1, 16, 0},
	"Quantidade":         {16, 24, 0},
	"MotivoCancelamento": {24, 26, 0},
}
