package retornoDePedido

import (
	"fmt"
	"time"
)

func GetHeaderRetorno(	CnpjCliente string, CodigoProjeto string, PedidoLaboratorio string) string{


	cabecalho := "1"
	cabecalho += fmt.Sprintf("%-14s", CnpjCliente)
	cabecalho += fmt.Sprintf("%-1s", CodigoProjeto)
	cabecalho += fmt.Sprintf("%-15s", PedidoLaboratorio )
	cabecalho += fmt.Sprintf("%02s", "" )

	return cabecalho
}

func GetDadosComplementaresRetorno(	Data time.Time, Hora time.Time) string{

	Dados := "2"
	Dados += Data.Format("02012006")
	Dados += Hora.Format("150405")
	Dados += fmt.Sprintf("%02s", "" )

	return Dados
}

func GetItensPedidoRetorno(EanProduto int64, QuantidadeAtendida int32, QuantidadeNaoAtendida int32, Motivo int32, Retorno int32  ) string{

	Itens := "3"
	Itens += fmt.Sprintf("%013d", EanProduto)
	Itens += fmt.Sprintf("%08d", QuantidadeAtendida)
	Itens += fmt.Sprintf("%08d", QuantidadeNaoAtendida)
	Itens += fmt.Sprintf("%02d", Motivo)
	Itens += fmt.Sprintf("%01d", Retorno)

	return Itens
}

func GetFimRetorno(QuantidadeUnidadesAtendidas int32, QuantidadeUnidadesNaoAtendidas int32, QuantidadeItensArquivo int32) string{

	Fim := "4"
	Fim += fmt.Sprintf("%08d", QuantidadeUnidadesAtendidas)
	Fim += fmt.Sprintf("%08d", QuantidadeUnidadesNaoAtendidas)
	Fim += fmt.Sprintf("%08d", QuantidadeItensArquivo)
	Fim += fmt.Sprintf("%012s", "" )

	return Fim
}
