package retornoDePedido

type Itens struct {
	TipoRegistro          int32 `json:"TipoRegistro"`
	EanProduto            int64 `json:"EanProduto"`
	QuantidadeAtendida    int32 `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int32 `json:"QuantidadeNaoAtendida"`
	Motivo                int32 `json:"Motivo"`
	Retorno               int32 `json:"Retorno"`
}
