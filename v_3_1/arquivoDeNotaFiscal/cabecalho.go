package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro            int32  `json:"TipoRegistro"`
	DataGeracaoArquivo      int32  `json:"DataGeracaoArquivo"`
	HoraGeracaoArquivo      int32  `json:"HoraGeracaoArquivo"`
	CnpjOperador            int64  `json:"CnpjOperador"`
	CodigoProjeto           string `json:"CodigoProjeto"`
	NumeroPedidoLaboratorio int32  `json:"NumeroPedidoLaboratorio"`
	NumeroPedidoCliente     string `json:"NumeroPedidoCliente"`
	Livre                   string `json:"Livre"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataGeracaoArquivo, "DataGeracaoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraGeracaoArquivo, "HoraGeracaoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjOperador, "CnpjOperador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoProjeto, "CodigoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedidoLaboratorio, "NumeroPedidoLaboratorio")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Livre, "Livre")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":            {0, 1, 0},
	"DataGeracaoArquivo":      {1, 9, 0},
	"HoraGeracaoArquivo":      {9, 15, 0},
	"CnpjOperador":            {15, 29, 0},
	"CodigoProjeto":           {29, 30, 0},
	"NumeroPedidoLaboratorio": {30, 37, 0},
	"NumeroPedidoCliente":     {37, 52, 0},
	"Livre":                   {52, 80, 0},
}
