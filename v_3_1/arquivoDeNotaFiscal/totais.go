package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Totais struct {
	TipoRegistro       int32   `json:"TipoRegistro"`
	ValorFrete         float32 `json:"ValorFrete"`
	ValorSeguro        float32 `json:"ValorSeguro"`
	OutrasDespesas     float32 `json:"OutrasDespesas"`
	ValorTotalProdutos float32 `json:"ValorTotalProdutos"`
	ValorTotalNF       float32 `json:"ValorTotalNF"`
	ValorIPI           float32 `json:"ValorIPI"`
	Livre              string  `json:"Livre"`
}

func (t *Totais) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTotais

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorFrete, "ValorFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorSeguro, "ValorSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.OutrasDespesas, "OutrasDespesas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorTotalProdutos, "ValorTotalProdutos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorTotalNF, "ValorTotalNF")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorIPI, "ValorIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Livre, "Livre")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTotais = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"ValorFrete":         {1, 9, 2},
	"ValorSeguro":        {9, 17, 2},
	"OutrasDespesas":     {17, 25, 2},
	"ValorTotalProdutos": {25, 33, 2},
	"ValorTotalNF":       {33, 41, 2},
	"ValorIPI":           {41, 49, 2},
	"Livre":              {49, 80, 0},
}
