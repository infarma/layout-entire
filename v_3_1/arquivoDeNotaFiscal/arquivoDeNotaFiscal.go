package arquivoDeNotaFiscal

import (
	"fmt"
	"strings"
	"time"
)

func GetCabecalho(DataGeracaoArquivo time.Time, HoraGeracaoArquivo time.Time, CnpjOperador int64, CodigoProjeto string, NumeroPedidoLaboratorio int32, NumeroPedidoCliente string) string{

	cabecalho := "1"
	cabecalho += DataGeracaoArquivo.Format("02012006")
	cabecalho += HoraGeracaoArquivo.Format("150405")
	cabecalho += fmt.Sprintf("%014d", CnpjOperador)
	cabecalho += fmt.Sprintf("%-1s", CodigoProjeto)
	cabecalho += fmt.Sprintf("%07d", NumeroPedidoLaboratorio)
	cabecalho += fmt.Sprintf("%-15s", NumeroPedidoCliente)
	cabecalho += fmt.Sprintf("%028s", "")

	return cabecalho
}

func GetDados( DataSaidaMercadoria time.Time, HoraSaidaMercadoria time.Time, DataEmissaoNotaFiscal time.Time, CnpjLoja string, NumeroNotaFiscal int32, SerieDocumento string, VencimentoNotaFiscal int32) string{

	dados := "2"
	dados += DataSaidaMercadoria.Format("02012006")
	dados += HoraSaidaMercadoria.Format("150405")
	dados += DataEmissaoNotaFiscal.Format("02012006")
	dados += fmt.Sprintf("%-14s", CnpjLoja)
	dados += fmt.Sprintf("%06d", NumeroNotaFiscal)
	dados += fmt.Sprintf("%-3s", SerieDocumento)
	dados += fmt.Sprintf("%08d", VencimentoNotaFiscal)
	dados += fmt.Sprintf("%026s", "")

	return dados
}

func GetTotais( ValorFrete float64, ValorSeguro float64, OutrasDespesas float64, ValorTotalProdutos float64, ValorTotalNF float64, ValorIPI float64) string{

	Totais := "3"
	Totais += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorFrete), ".", "", 1))
	Totais += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorSeguro), ".", "", 1))
	Totais += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", OutrasDespesas), ".", "", 1))
	Totais += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorTotalProdutos), ".", "", 1))
	Totais += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorTotalNF), ".", "", 1))
	Totais += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorIPI), ".", "", 1))
	Totais += fmt.Sprintf("%031s", "")

	return Totais
}

func GetICSM(	BaseCalculoICMS float64, ValorICMS float64, BaseCalculoSubstituicaoTributaria float64, ValorICMSSubstituicaoTributaria float64) string{

	ICMS := "4"
	ICMS += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", BaseCalculoICMS), ".", "", 1))
	ICMS += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorICMS), ".", "", 1))
	ICMS += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", BaseCalculoSubstituicaoTributaria), ".", "", 1))
	ICMS += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorICMSSubstituicaoTributaria), ".", "", 1))

	ICMS += fmt.Sprintf("%047s", "")

	return ICMS
}

func GetItens( CodigoEan string, CodigoProdutoOperador string, Quantidade int32, Unidade float64, PrecoFabrica float64, DescontoComercial float64, ValorDescontoComercial float64, ValorRepasse float64, Repasse float64, ValorUnitario float64, Fracionamento float64,) string{

	itens := "5"
	itens += fmt.Sprintf("%-13s", CodigoEan)
	itens += fmt.Sprintf("%-7s", CodigoProdutoOperador)
	itens += fmt.Sprintf("%04d", Quantidade)
	itens += fmt.Sprintf("%03s", strings.Replace(fmt.Sprintf("%.2f", Unidade), ".", "", 1))
	itens += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", PrecoFabrica), ".", "", 1))
	itens += fmt.Sprintf("%04s", strings.Replace(fmt.Sprintf("%.2f", DescontoComercial), ".", "", 1))
	itens += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorDescontoComercial), ".", "", 1))
	itens += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorRepasse), ".", "", 1))
	itens += fmt.Sprintf("%04s", strings.Replace(fmt.Sprintf("%.2f", Repasse), ".", "", 1))
	itens += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorUnitario), ".", "", 1))
	itens += fmt.Sprintf("%04s", strings.Replace(fmt.Sprintf("%.2f", Fracionamento), ".", "", 1))

	itens += fmt.Sprintf("%08s", "")

	return itens
}

func GetFim( NumeroItensNotaFiscal int32) string{

	Fim := "6"
	Fim += fmt.Sprintf("%04d", NumeroItensNotaFiscal)
	Fim += fmt.Sprintf("%075s", "")

	return Fim
}