package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Icms struct {
	TipoRegistro                      int32   `json:"TipoRegistro"`
	BaseCalculoICMS                   float32 `json:"BaseCalculoICMS"`
	ValorICMS                         float32 `json:"ValorICMS"`
	BaseCalculoSubstituicaoTributaria float32 `json:"BaseCalculoSubstituicaoTributaria"`
	ValorICMSSubstituicaoTributaria   float32 `json:"ValorICMSSubstituicaoTributaria"`
	Livre                             string  `json:"Livre"`
}

func (i *Icms) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIcms

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorICMS, "ValorICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.BaseCalculoSubstituicaoTributaria, "BaseCalculoSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorICMSSubstituicaoTributaria, "ValorICMSSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Livre, "Livre")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIcms = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"BaseCalculoICMS":                   {1, 9, 2},
	"ValorICMS":                         {9, 17, 2},
	"BaseCalculoSubstituicaoTributaria": {17, 25, 2},
	"ValorICMSSubstituicaoTributaria":   {25, 33, 2},
	"Livre":                             {33, 80, 0},
}
